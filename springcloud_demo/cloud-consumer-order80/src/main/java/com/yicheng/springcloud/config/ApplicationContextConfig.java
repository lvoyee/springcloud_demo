package com.yicheng.springcloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ApplicationContextConfig {

    @Bean
//    @LoadBalanced // 测试自定义 负载均衡规则时 记得注释这个注解
    public RestTemplate getRestTemplate(){
        return new RestTemplate();
    }
}
