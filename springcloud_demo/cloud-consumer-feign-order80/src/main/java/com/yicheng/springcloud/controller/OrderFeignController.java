package com.yicheng.springcloud.controller;

import com.yicheng.springcloud.common.Result;
import com.yicheng.springcloud.service.OrderFeignService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class OrderFeignController {

    @Resource
    private OrderFeignService orderFeignService;

    @GetMapping(value = "/payment/get/{id}")
    public Result getPaymentById(@PathVariable("id") Long id){
        return orderFeignService.getPaymentById(id);
    }

    @GetMapping(value = "/payment/feign/timeout")
    public String getFeignTimeOut(){
        return orderFeignService.getFeignTimeOut();
    }

}
