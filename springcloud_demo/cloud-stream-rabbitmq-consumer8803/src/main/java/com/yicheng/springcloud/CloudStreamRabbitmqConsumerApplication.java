package com.yicheng.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CloudStreamRabbitmqConsumerApplication {
    public static void main(String[] args) {
        SpringApplication.run(CloudStreamRabbitmqConsumerApplication.class,args);
    }
}
