package com.yicheng.springcloud.service;


import com.yicheng.springcloud.common.Result;
import com.yicheng.springcloud.entities.Payment;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


@FeignClient(value = "nacos-payment-provider",fallback = PaymentFallbackService.class)
public interface PaymentService
{
    @GetMapping(value = "/paymentSQL/{id}")
    Result<Payment> paymentSQL(@PathVariable("id") Long id);
}
