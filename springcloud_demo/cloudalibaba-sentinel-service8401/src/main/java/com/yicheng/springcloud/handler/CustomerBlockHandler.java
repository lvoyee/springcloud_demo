package com.yicheng.springcloud.handler;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.yicheng.springcloud.common.Result;

public class CustomerBlockHandler {

    public static Result handlerException(BlockException exception){
        return new Result(2020,"自定义限流处理消息 ....");
    }
}
