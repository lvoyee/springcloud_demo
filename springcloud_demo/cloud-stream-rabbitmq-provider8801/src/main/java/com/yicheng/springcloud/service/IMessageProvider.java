package com.yicheng.springcloud.service;

public interface IMessageProvider {
    String send();
}
