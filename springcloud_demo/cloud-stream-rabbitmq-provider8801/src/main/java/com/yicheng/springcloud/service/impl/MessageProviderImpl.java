package com.yicheng.springcloud.service.impl;

import com.yicheng.springcloud.service.IMessageProvider;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.MessageChannel;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * 消息发送 = 生产者
 * @EnableBinding(Source.class) 一个注解 + Source
 * output                        消息发送
 */
@EnableBinding(Source.class)
public class MessageProviderImpl implements IMessageProvider {

    @Resource
    private MessageChannel output;// 消息发送管道·

    @Override
    public String send() {
        output.send(MessageBuilder.withPayload(UUID.randomUUID().toString()).build());
        System.out.println("生产者 消息发送："+UUID.randomUUID().toString());
        return null;
    }
}
