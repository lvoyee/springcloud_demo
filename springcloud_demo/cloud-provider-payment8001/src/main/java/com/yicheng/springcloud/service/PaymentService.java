package com.yicheng.springcloud.service;

import com.yicheng.springcloud.entities.Payment;

public interface PaymentService {

    int create(Payment payment);

    Payment getPaymentById(Long id);
}
