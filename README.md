# springcloud_demo

#### 介绍
个人springcloud系统学习笔记demo

#### 说明
学习代码来源 尚硅谷SpringCloud框架开发教程(SpringCloudAlibaba微服务分布式架构丨Spring Cloud) 视频


学习地址： https://www.bilibili.com/video/BV18E411x7eT?p=1&vd_source=857c09f64774965075bb71876d9dae79


#### 学习记录

```
<dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-dependencies</artifactId>
        <version>2.2.2.RELEASE</version>
        <type>pom</type>
        <scope>import</scope>
</dependency>
<dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-dependencies</artifactId>
        <version>Hoxton.SR1</version>
        <type>pom</type>
        <scope>import</scope>
</dependency>
```

Spring Boot 版本： 2.2.2.RELEASE

Spring Cloud版本： Hoxton.SR1

1.payment表

```
DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment`
(
    `id`     bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
    `serial` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '支付流水号',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '支付表' ROW_FORMAT = Dynamic;
```

```
INSERT INTO `payment`
VALUES (1, '202208251140156119058');
```

2.热部署插件个人没有安装，不太需要这玩意。

3.开启 idea(版本：2020.3) Run Dashboard功能
找到项目对应的.idea里的workspace.xml文件，然后在文件后面追加以下内容即可：
```
<component name="RunDashboard">
    <option name="configurationTypes">
      <set>
        <option value="SpringBootApplicationConfigurationType" />
      </set>
    </option>
  </component>
 ```

不需要重启idea ！

4.引用公共api-common
```
<dependency>
    <groupId>com.lvoyee.springcloud</groupId>
    <artifactId>cloud-api-common</artifactId>
    <version>${project.version}</version>
    <scope>compile</scope>
</dependency>
 ```

tips: groupId 就是新创建的api-common 的 groupId

artifactId 也可以参考新创建的api-common中的pom.xml文件

对于version 可以固定写死为 ${project.version}

5.eureka使用需要注意的点
使用 2.X 需要引入以下依赖 不要引入老版本的依赖
```
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-eureka-server</artifactId>
</dependency>
```

集群配置需要需要的点,任意一个eureka配置的不正确，假如我这里是配置以下这样
```
# spring cloud
127.0.0.1 eureka7001.com
127.0.0.1 eureka7002.com
```

如果7001的 application.yml 文件 没有配置对 ，访问 eureka7001.com:7001 是无法看到 eureka7002.com 的！

6. P22中涉及的cloud-provider-payment8001 要弄集群，视频中是新建了一个项目（应该是教学演示）。
实际中，可以 Services 面板选择对应的服务 右键 - copy configuration - 输入名字 ，添加端口。


```
-Dserver.port=8002
```

7.cloud-provider-payment集群配置，在order服务中的请求地址需要变更为服务名称（集群环境）P22

8.P24 没有跟着做！感觉没啥用 但是 注解 @EnableDiscoverClient 很重要！

9.euraka自我保护机制的关闭 P26

```
# 服务端
eureka.server.enable-self-preservation=true

# 客户端 时间改短一点
eureka.instance.lease-renewal-interval-in-seconds=30
eureka.instance.lease-expiration-duration-in-seconds=90
```

关闭自我保护机制后，服务一旦不可用根据客户端配置的时间，到时间eureka服务端就会把不可用的服务剔除。

10.关于zookeeper的问题

路径 /usr/local/zookeeper-3.5.10

在复制的 zoo.cfg 文件里面记得配置

```
# 禁用 AdminServer 服务
admin.enableServer=false
```

不然无法使用 ./zkCli.sh 连接上zookeeper 的服务端


P28 中出现的jar冲突 ， 我这里使用的 zookeeper-3.5.10 没有这个问题

但是 需要配置 数据库连接，不然启动不了

报错信息如下：
```
Failed to configure a DataSource: ‘url‘ attribute is not specified and no em
```

所以 cloud-provider-payment8004 项目中我的 pom.xml 
```
#8004表示注册到zookeeper服务器的支付服务提供者端口号
server:
  port: 8004


#服务别名----注册zookeeper到注册中心名称
spring:
  application:
    name: cloud-provider-payment
  cloud:
    zookeeper:
      connect-string: 192.168.191.133:2181
  datasource:
    type: com.alibaba.druid.pool.DruidDataSource
    driver-class-name: org.gjt.mm.mysql.Driver
    url: jdbc:mysql://localhost:3306/cloud_db?useUnicode=true&characterEncoding=utf-8&usessL=false
    username: root
    password: root
```

11.小小的小结
eureka - zookeeper - consul

AP     -     CP    -  CP

12.P36 ribbon 的学习
ribbon 负载均衡（客户端负载均衡的工具）。

总结: Ribbon其实就是一个软负载均衡的客户端组件,  他可以和其他所需请求的客户端结合使用,和eureka结合只是其中一个实例。

默认的负载均衡 （轮询）
```
package com.yicheng.springcloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ApplicationContextConfig {

    @Bean
    @LoadBalanced
    public RestTemplate getRestTemplate(){
        return new RestTemplate();
    }
}
```

Tips: 运行自定义轮询规则的时候 记得 把@LoadBalanced 注解注释！


13.服务降级 P54 P55 P56 P57

Hystrix 在controller方法上添加 
```
    @HystrixCommand(fallbackMethod = "paymentInfo_TimeOutHandler",
            commandProperties = {
                    @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",value = "3000")
            })
```

这种方式，等于每个接口都要写对应的一个降级方法会导致 代码膨胀！还导致了业务代码和非业务代码混乱在一块。

第二种方式 @DefaultProperties(defaultFallback="") 不需要每个接口方法都写一个 指定一个全局的异常fall back即可，but 还是不够优雅

第三种方式 @FeignClient(value = "CLOUD-PROVIDER-HYSTRIX-PAYMENT",fallback = PaymentFallbackService.class)
通过新建一个fallback类用结合feignclient和fallback属性新建每个接口兜底方法，这样业务代码和fall back就分开了。

14.P58 服务熔断

```
 @HystrixCommand(fallbackMethod = "paymentCircuitBreakerFallback", commandProperties = {
            @HystrixProperty(name = "circuitBreaker.enabled", value = "true"),/* 是否开启断路器*/
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "10"),// 请求次数
            @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "10000"), // 时间窗口期
            @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "60"),// 失败率达到多少后跳闸
//            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000")// 超时处理
    })
```
通过以上的注解进行 服务熔断

15.P62 服务限流

这里先占个坑 后续补

16.P66 gateway新一代网关
spring boot 和 spring cloud 版本关系如果不对应 新建的gateway是无法启动的！

动态路由： 通过服务名实现

Predicate 断言 11种

Filter 多种路由过滤器 都是由GateWayFilter工厂类产生的

16.服务配置 & 服务总线 P74

Config  分布式配置中心 分为服务端和客服端。

Springcloud Config分为服务端和客户端两部分。
 
服务端也称分布式配置中心，它是一个独立的微服务应用，用来连接配置服务器并未客户端提供获取配置信息，加密、解密信息等访问接口。

作用：

集中管理配置文件

不同环境不同配置，动态化的配置更新，分环境部署比如dev/test/prod/beta/release

运行期间动态调整配置，不再需要字啊每个服务器的机器上编写配置文件，服务会向配置中心同意拉去配置自己的信息

当配置发生变动时，服务不需要重启即可感知到配置的变化并应用新的配置

将配置信息以REST接口的形式暴露

P75 spring cloud config 的知识点

config客户端动态刷新配置文件

```
curl -X POST "http://localhost:3355/actuator/refresh"
```

只通知一个节点

```
curl -X POST "http://localhost:3344/actuator/bus-refresh/config-client:3355"
```

17.P83 spring cloud strem 知识点

一句话：屏蔽底层消息中间件的差异，降低切换版本，统一消息的编程模型。
目前只支持 rabbitmq和Kafka !

binder  很方便的连接中间件，屏蔽差异

Channel 通道，是队列Queue的一种抽象，在消息通讯系统中就是实现存储和转发的媒介，通过对Channel对队列进行配置

Source和Sink  简单的可理解为参照对象是Spring Cloud Stream自身，从Stream发布消息就是输出，接受消息就是输入

消息持久化关键的配置

```
cloud:
    stream:
      binders: # 在此处配置要绑定的rabbitmq的服务信息；
        defaultRabbit: # 表示定义的名称，用于于binding整合
          type: rabbit # 消息组件类型
          environment: # 设置rabbitmq的相关的环境配置
            spring:
              rabbitmq:
                host: localhost
                port: 5672
                username: guest
                password: guest
      bindings: # 服务的整合处理
        input: # 这个名字是一个通道的名称
          destination: studyExchange # 表示要使用的Exchange名称定义
          content-type: application/json # 设置消息类型，本次为json，文本则设置“text/plain”
          binder: defaultRabbit  # 设置要绑定的消息服务的具体设置
          group: MessageA # 这个group 狠重要
```

18.P92 Sleuth分布式请求链路追踪 

SpringCloud从F版起已不需要自己构建Zipkin server了，只需要调用jar包即可

zipkin 下载地址：https://repo1.maven.org/maven2/io/zipkin/zipkin-server/2.14.1/

19.Spring cloud nacos

tips:（优先级）

bootstrap配置文件优先级高于application配置文件

nacos 配置

```
公式:${spring.application.name}-${spring.profile.active}.${spring.cloud.nacos.config.file-extension}

prefix                  默认为 spring.application.name 的值
spring.profile.active   既为当前环境对应的 profile ,可以通过配置项 spring.profile.active 来配置
file-exetension         为配置内容的数据格式，可以通过配置项 spring.cloud.nacos.config.file-extension 配置
```

--- 

nacos 配置列表 新增 配置的时候 如果选择的是 yaml 格式 一定要注意冒号后面带一个空格，注意缩进！否则识别不了对应的配置信息！！

P101 nacos 配置中心 - 也实现了动态刷新配置信息的功能！

多环境多项目管理的三种方案

1.DataID方案

指定spring.profile.active和配置文件的DataID来使不同环境下读取不同的配置，默认空间+默认分组+新建dev和test两个DataID。

2.Group方案

通过Group实现环境区分，在nacos图形界面控制台上面新建配置文件DataID

3.Namespace方案

新建dev/test的Namespace 按照域名配置填写!


nacos 集群配置

最少要配置三个nacos

预计需要，1个nginx+3个nacos注册中心+1个mysql（实际生成环境中1个nginx需改为多个，一个mysql也需要改为多个【主从】）

20.P111 开始学习spring cloud sentinel

后台
https://github.com/alibaba/Sentinel/releases

sentinel-dashboard-1.7.0.jar

- 流控规则

- 降级规则

- 热点key限流

- 系统规则

- @SentinelResource注解

- 服务熔断功能

- 规则持久化

21.P138 分布式事务（springcloud alibaba seata）-- P148
 
对应的笔记就不记录了！

2022-09-16